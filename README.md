# Projet PCSEA RISC-V

Point d'entré du projet sur le wiki <https://gitlab.ensimag.fr/pcserv/documentation-risc-v/wikis>

## Dossiers du projet

* src/ : Le code du kernel et de l'espace user ***Readme spécifique***
* exemple/ : Une série de petits exemples qui permet également de vérifier l'environnement de travail
* docker/ : Image Docker de l'environnement de cross-compilation ***Readme spécifique***

## Mode d'emploi

Lancer les tests kernel:

```sh
make clean KTESTS=TEST_KERNEL dk-go
```

Lancer les tests user:

```sh
make clean KTESTS=TEST_USER dk-go
```

## Avancement du projet

- [x] Phase 1: environnement
- [x] Phase 2: création et lancement de processus kernel
- [x] Phase 3: ordonnancement, création dynamique et terminaison de processus kernel
- [x] Phase 4: files de messages
- [ ] Phase 5: séparation mode user
- [x] Phase 6: gestion clavier et pilote console
- [ ] Phase 7: implémentation d'un shell

### Tests user importé aux kernel

- Tests qui passent : test0, test1, test2 , test3, test4, test8, test10, test12
- Tests qui échouent (avec les implémentations requises) : test5
- Tests qui échouent (par manque d'implémentations requises) : test7, test11, test13

## Ce qui est fait

* Implementation du timer
* Implementation du passage en mode supervisor
* Implementation de la mémoire virtuelle
* Implementation des processus
* Implementation du changement de contexte et du scheduler
* Portage des tests user dans le kernel
* Implementation des files de messages
* Implementation du traitant d'interruption clavier
* Implementation de l'espace user et du passage en mode user
* Implementation de la mémoire partagée (sur la branche shared_memory uniquement)

## Reste à faire

* Merge la branche test_5 où qq petits fixs ont été faits sur procs.c mais pas eu le temps de merge à la fin
* Reussir les tests du mode user (autotest)
* Terminer les syscalls

Pour plus d'information, lire les Readme spécifiques ou rendez-vous sur le [wiki](https://gitlab.ensimag.fr/pcserv/documentation-risc-v/wikis).

