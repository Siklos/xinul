/*
 * Projet PCSEA RISC-V
 *
 * Benoît Wallon <benoit.wallon@grenoble-inp.org> - 2019
 * Mathieu Barbe <mathieu@kolabnow.com> - 2019
 *
 * See license for license details.
 */
#include "stdlib.h"
#include "assert.h"

#include "encoding.h"
#include "riscv.h"
#include "info.h"
#include "trap.h"


// Prototypes externes
// kernel/start.c
extern int kernel_start();

// kernel/boot/$(MACHINE)/setup.c
extern void arch_setup();

static void delegate_traps()
{
// a faire, déléguer interruption timer supervisor
	// 2s. Le processeur voit que l'interruption STI est déléguée au mode supervisor.
	csr_set(mideleg, MIP_STIP);
	// Delegate 8th case/bit: U environment call
	csr_set(medeleg, 0x0100);

// Puis à la fin du projet déléguer les exception pour les pages faults
}

static inline void setup_pmp(void)
{
	// A faire
}

__attribute__((noreturn)) static inline void enter_supervisor_mode()
{
	setup_pmp();

	// Changing the privilege mode requires to change the status register (mstatus here)
	// Actually, we only change the xPP (Previous Privilege) to the correct value (see Privilege Manual p22)
	csr_clear(mstatus, MSTATUS_MPP); // Clear MPP to be sure
	csr_set(mstatus, MSTATUS_MPP_S); // Set MPP to the correct value

	// Set SPP to a correct value
	csr_set(mstatus, MSTATUS_SPP);
	
	// Set SPP to a correct value
	csr_set(mstatus, MSTATUS_SUM);

	// We need to jump correctly here, to hit the end of the function.
	csr_write(mepc, kernel_start);

	// MRET instruction uses mepc CSR to jump correctly, we thus need to set it to the correct location
	__asm__ __volatile__ ("la ra, exit");
	MRET;
}

/*
 * boot_riscv
 *
 * Cette fonction est appelée depuis crtm.S
 * seulement le vecteur de trap machine est configuré
 * Le processeur est encore en mode machine
 */
__attribute__((noreturn)) void boot_riscv()
{
	// Configuration spécifique à la machine utilisé, (uart / htif).
	arch_setup();

	display_info_proc();

	// Activation des interruption machine timer
// a faire
	// Ligne suivante a supprimer apres test : mstatus <- 1
	csr_set(mstatus, MSTATUS_MIE);
	// 4m. Le processeur vérifie que l'interruption timer machine soit démasquée à partir du champ MTIE du registre csr mie.
	csr_set(mie, MIP_MTIP);

	csr_set(mie, MIP_MEIP);
	csr_set(mie, MIP_SEIP);

	set_mtimecmp(clint_dev->clk_freq);

	// 3s. Le processeur vérifie que les interruptions soit globalement activées pour le mode supervisor.
	ENABLE_SUPERVISOR_INTERRUPTS();
	// 4s. Le processeur vérifie que l'interruption supervisor timer soit démasquée.
	csr_set(mie, MIP_STIP);

	

	delegate_traps();

	// Configuration de supervisor trap vector (direct mode bit 0 à 0)
	// a faire


	enter_supervisor_mode();
	__builtin_unreachable();
}
