//
// Created by Mathieu Barbe on 21/01/2019.
//

#ifndef SRC_CONS_WRITE_H
#define SRC_CONS_WRITE_H

int cons_write(const char *str, long size);

#endif //SRC_CONS_WRITE_H
