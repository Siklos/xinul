#include "terminal.h"
#include "string.h"
#include "stdio.h"
#include "riscv.h"

static char buffer[255] = { 0 };

static int allow_echo = 1;

static int initial_char_index = 0;
static int current_char_index = 0;

void uart_interrupt_handler(char c) {
	if (allow_echo) {
		if (c != 13) {
			printf("%c", c);
		} else {
			printf("\n");
		}
	}
	buffer[current_char_index] = c;
	current_char_index = (current_char_index + 1) % 255;
}


void cons_echo(int on) {
	allow_echo = on;
}


unsigned long cons_read(char *string, unsigned long length) {
	if (length == 0) return 0;

	while(buffer[current_char_index - 1] != 13) {
		wfi();
	}

	int string_size = current_char_index - initial_char_index;
	if (length >= string_size) {
		length = string_size - 1;
	}

	for (int i = 0; i < length; i++) {
		string[i] = buffer[(initial_char_index + i) % 255];
		buffer[(initial_char_index + i) % 255] = '\0';
	}
	initial_char_index = (initial_char_index + length) % 255;
	return string_size;
}
