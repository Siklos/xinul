#include "timer.h"

#include "riscv.h"
#include "encoding.h"
#include "device.h"
#include "stdio.h"

#include "sbi.h"

#include "proc.h"

static unsigned long timer_interrupt_counter = 0;

void handle_mtimer_interrupt()
{

	/*
	 * Todo: Reconfigurer le timer machine à delta ms dans le future et notifier par interruption l'espace noyau )supervisor)
	 *
	 * 1. Reconfiguration du timer machine dans le future à l'aide de la macro set_mtimecmp (riscv.h).
	 * 2. Activer l'interruption timer du supervisor.
	 * 3. Si SIT a belle et bien été déléguer et activé, une interruption au niveau du supervisor devrait avoir lieu.
	 */

	// 6. Le traitant découvre que la trap provient du timer machine.
	timer_interrupt_counter++;
	// 8. Le traitant lève une interruption à destination du mode supervisor à l'aide du registre mip.
	csr_set(mip, MIP_STIP);

	// 9.Fin du traitant timer machine. On rend la main au programme.
		// 7. Le traitant du timer machine met à jour mtimecmp pour programmer une nouvelle interruption dans le future. 
		// L'écriture de mtimecmp a pour effet d'acquitter l'interruption timer machine.
	set_mtimecmp(get_mtime() + clint_dev->clk_freq);
}

void handle_stimer_interrupt()
{
	sbi_call_ack_timer_interrupt();
	schedule();
}

void clock_settings(unsigned long *quartz, unsigned long *ticks)
{
	*ticks = clint_dev->clk_freq;
	*quartz = 1 / clint_dev->clk_freq;
}

unsigned long current_clock()
{
	return timer_interrupt_counter;
}