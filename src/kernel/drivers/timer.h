#pragma once
	/**
 * Machine interruption handler
 * 
 * It will : 
 *  - (Do stuff like print a message)
 *  - Make a supervisor interruption call
 *  - Reschedule mtimecmp to a later date
 *  - Ack the interruption by setting MTIP bit to 0
 */
	void handle_mtimer_interrupt();

/**
 * Supervisor interruption handler
 * 
 * It will : 
 *  - (Do stuff like print a message)
 *  - Ack the interruption by setting STIP bit to 0
 *    by using the Supervisor Binary Interface (SBI)
 */
void handle_stimer_interrupt();

void clock_settings(unsigned long *quartz, unsigned long *ticks);

unsigned long current_clock();