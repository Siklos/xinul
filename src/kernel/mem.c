/*
 * Copyright (C) 2005 -- Simon Nieuviarts
 * Copyright (C) 2012 -- Damien Dejean <dam.dejean@gmail.com>
 *
 * Kernel memory allocator.
 */
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include "mem.h"
#include "stddef.h"
#include "encoding.h"

/* Heap boundaries */
extern const char _heap_start[];
extern const char _heap_end[];
extern const char _free_memory_start[];
extern const char _memory_end[];
/* static char *curptr = _free_memory_start; */

/* Free Zone Table */
#define FZT_SIZE (RISCV_PGSHIFT + 9) // This is a megapage
#define BLOCK_VALUE(blk) *((void **)(blk))
#define MAX_ALLOC_SIZE RISCV_PGSIZE
#define MIN_ALLOC_SIZE 0x40
#define BUDDY_NOT_FOUND ((void *)-1)
static void * FZT[FZT_SIZE] = { NULL };

//
// KERNEL MEMORY ALLOCATOR
//

static inline void push_to_FZT(void * address, unsigned int index)
{
	// link to previous head
	BLOCK_VALUE(address) = FZT[index];

	// Push into list
	FZT[index] = address;
}

static inline void * pop_from_FZT(unsigned int index)
{
	// Save old head
	void * current_head = FZT[index];

	// Pop it
	void * future_head = BLOCK_VALUE(FZT[index]);
	FZT[index] = future_head;

	BLOCK_VALUE(current_head) = NULL;

	return current_head;
}

/* Trivial sbrk implementation */
void * sbrk(ptrdiff_t diff)
{
	return NULL;
	/* char *s = curptr; */
	/* char *c = s + diff; */
	/* if ((c < curptr) || (c > _memory_end)) return ((void*)(-1)); */
	/* curptr = c; */
	/* return s; */
}

static unsigned int pow2(unsigned long size)
{
	unsigned int p = 0;
	size = size - 1; // allocation start in 0
	while (size) {  // get the largest bit
		p++;
		size >>= 1;
	}
	if (size > (1 << p))
		p++;
	return p;
}

static void split(unsigned int index_start)
{
	if ( index_start == FZT_SIZE ) {
		// No more memory
		return;
	} else if ( FZT[index_start] == NULL ) {
		// Drop the output of split for now
		split(index_start + 1);
	}

	// If there is no more memory, split may fail, thus check it
	if ( FZT[index_start] != NULL ) {
		// Relink chained list
		void * current_head = pop_from_FZT(index_start);

		// Split current block
		unsigned long block_size = 1 << (index_start - 1);
		void * first_block_address = current_head;
		void * second_block_address = current_head + block_size;

		BLOCK_VALUE(second_block_address) = NULL;

		// Link the two blocks
		push_to_FZT(second_block_address, index_start - 1);
		push_to_FZT(first_block_address, index_start - 1);
	}
}

/**
 * Allocates length bytes in memory
 *
 * Internaly, it uses the buddy algorithm, with powers of 2
 */
void * kmalloc(unsigned long length)
{
	// Gather the highest power of 2 containing length
	unsigned long index = pow2((length > MIN_ALLOC_SIZE) ? length : MIN_ALLOC_SIZE);

	// Normalize the allocation size, to be in the correct range
	if ( index >= FZT_SIZE ) return NULL;

	if ( FZT[index] == NULL ) {
		split(index + 1);
	}

	if ( FZT[index] == NULL ) {
		return NULL;
	} else {
		char * zone = pop_from_FZT(index);

        // Initialize to 0
        for (unsigned int i = 0; i < length; i++) zone[i] = 0;

        return zone;
	}
}

static void * get_buddy_if_possible(void * block, unsigned long size)
{
	// Compute buddy and index in table
	void * buddy_address = (void *)((unsigned long)block ^ size);
	unsigned int index_tzl = pow2(size);

	// Try to find buddy in not allocated list
	void * current = FZT[index_tzl];
	void * prec = NULL; // We will have to pop buddy from the list

	while ( current != NULL ) {
		if ( current == buddy_address ) {
			// Pop buddy
			if ( prec != NULL ) {
				// If we've done some loop turns
				// we relink everything
				BLOCK_VALUE(prec) = BLOCK_VALUE(current);
			} else {
				// If we don't buddy was in the beginning of the
				// list, thus we have to pop it
				pop_from_FZT(index_tzl);
			}

			// Return buddy
			return current;
		}

		prec = current;
		current = BLOCK_VALUE(current);
	}

	// If we don't find it, return a default value
	return BUDDY_NOT_FOUND;
}

static void fuse_recursively(void * block_a, void * block_b, unsigned long size)
{
	// Get the highest address between the two block
	void * high_address = (block_a > block_b) ? block_b : block_a;
	void * next_buddy = get_buddy_if_possible(high_address, size);

	// If there is no more buddies
	if ( next_buddy == BUDDY_NOT_FOUND ) {
		// Fuse the blocks
		push_to_FZT(high_address, pow2(size));
	} else {
		// Fuse buddy and higher block
		fuse_recursively(high_address, next_buddy, size << 1);
	}
}

void kfree(void *zone, unsigned long length)
{
	// We need to initialize the fusion process
	// thus there is some code duplication here
	void * buddy = get_buddy_if_possible(zone, length);

	if ( buddy != BUDDY_NOT_FOUND ) {
		fuse_recursively(zone, buddy, length << 1);
	} else {
		// Just push it into the list
		push_to_FZT(zone, pow2(length));
	}
}

//
// FRAME ALLOCATOR
//
// Frame space is located between _free_memory_start and _memory_end

// A chained list of the free frames
static void * free_zones;

void * alloc_frame()
{
	if ( free_zones == NULL ) return NULL;

	void * allocd = free_zones;

	free_zones = BLOCK_VALUE(free_zones);

	return allocd;
}

void free_frame(void *zone)
{
	// Simply push the freed frame top free_zones list.
	assert(((uint64_t)zone) % RISCV_PGSIZE == 0);
	BLOCK_VALUE(zone) = free_zones;
	free_zones = zone;
}

//
// GENERICS
//

/**
 * Initializes kernel memory allocator, must be called before any alloc
 */
void init_allocator()
{

	//
	// Initialize kernel memory
	//
	// As the bigest block we may allocate is a kilopage, we initialize FZT
	// with all the 4KiB blocks at FZT[FZT_SIZE - 1]
	void * prec = (void *)_heap_start;
	FZT[FZT_SIZE - 1] = prec;

	uint64_t nr_pgs = ((uint64_t)_heap_end - (uint64_t)_heap_start) >> FZT_SIZE;

	// Create the block chained list
	// To save us some memory, next block address is stored directly in current block
	for (uint64_t pg_index = 1; pg_index < nr_pgs; pg_index++) {
		void * current = prec + (1 << FZT_SIZE);

		BLOCK_VALUE(prec) = current;

		prec = current;
	}

	//
	// Initialize frame allocator
	//
	prec = (void *)_free_memory_start;
	free_zones = (void *)_free_memory_start;

	uint64_t free_pages_nr = ((uint64_t)_memory_end - (uint64_t)_free_memory_start) >> RISCV_PGSHIFT;
	for (uint64_t pg_index = 1; pg_index < free_pages_nr; pg_index++) {
		void * current = prec + RISCV_PGSIZE;

		BLOCK_VALUE(prec) = current;

		prec = current;
	}
}
