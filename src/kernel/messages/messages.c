#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "mem.h"
#include "queue.h"
#include "proc.h"

#include "messages.h"

typedef struct pqueue {
	uint64_t fid;
    size_t count;           /* number of msg to distribute */

    size_t buffer_len;
    size_t cursor_read;
    size_t cursor_write;
    message_t *buffer;      /* buffer of msg to distribute */

    link procs;             /* FIFO waiting process */
    int nb_procs;           /* number of process currently waiting */

    bool reset;
} pqueue_t;

/** Table of available message queues */
pqueue_t *pqueue_table[NBQUEUE];

/* Check for valid fid and return -1 if error */
#define CHECK_FID(fid) { \
    if (!((fid) >= 0 && (fid) < NBQUEUE && pqueue_table[(fid)] != NULL)) { \
        return -1; \
    }}

#define CHECK_RESET(queue) { \
    if (queue->reset) return -1; \
}

/* utilities */
static pqueue_t *get_pqueue(fid_t);
static bool is_full(pqueue_t *);
static bool is_empty(pqueue_t *);
static void add_msg(pqueue_t *, message_t);
static message_t pop_msg(pqueue_t *);
static void _reset(pqueue_t *);
static fid_t get_free_fid();

/* ========================================================================= */

fid_t pcreate(int count) {
	fid_t fid = get_free_fid();
	if (fid == -1) {
		return -1;
	}

    // init struct
    pqueue_t *q = kmalloc(sizeof(pqueue_t));
    if (q == NULL) {
        return -1;
    }

    // init buffer
	q->fid = fid;
    q->count = count;
    q->buffer_len = 0;
    q->cursor_read = 0;
    q->cursor_write = 0;
    q->buffer = kmalloc(count * sizeof(message_t));
    if (q->buffer == NULL) {
        kfree(q, sizeof(pqueue_t));
        return -1;
    }

    // init FIFO
    INIT_LIST_HEAD(&q->procs);
    q->nb_procs = 0;

	// insert in table
	pqueue_table[q->fid] = q;

    return q->fid;
}

int pdelete(fid_t fid) {
    CHECK_FID(fid);
    pqueue_t *q = get_pqueue(fid);
    _reset(q);
	pqueue_table[fid] = NULL;
    kfree(q, sizeof(pqueue_t));
    return 0;
}

int psend(fid_t fid, message_t message) {
    CHECK_FID(fid);
    pqueue_t *q = get_pqueue(fid);

    if (! queue_empty(&q->procs)) {
		add_msg(q, message);

        q->nb_procs--;
        io_release(&q->procs);
    }
    else if (! is_full(q)) {
        add_msg(q, message);
    }
    else {
        // block
        q->nb_procs++;
        io_block(&q->procs);

        // here after wake
        CHECK_RESET(q);
        add_msg(q, message);
    }
    
    return 0;
}

int preceive(fid_t fid, message_t *message) {
    CHECK_FID(fid);
    pqueue_t *q = get_pqueue(fid);

    if (! is_empty(q)) {
        bool was_full  = is_full(q);
        // transmit message to process
        *message = pop_msg(q);

        if (was_full && q->nb_procs > 0) {
            q->nb_procs--;
            io_release(&q->procs);
        }
    }
    else {
        q->nb_procs++;
        io_block(&q->procs);

        // here after wake
        CHECK_RESET(q);
        *message = pop_msg(q);
    }

    return 0;
}

int preset(fid_t fid) {
    CHECK_FID(fid);
    pqueue_t *q = get_pqueue(fid);
    _reset(q);
    return 0;
}

int pcount(fid_t fid, int *count) {
    CHECK_FID(fid);
    pqueue_t *q = get_pqueue(fid);

    if (count != NULL) {
        if (is_empty(q)) {
            *count = - q->nb_procs;
        }
        else {
            *count = q->buffer_len + q->nb_procs;
        }
    }

    return 0;
}

/* ========================================================================= */

static pqueue_t *get_pqueue(fid_t fid) {
    return pqueue_table[fid];
}

static bool is_empty(pqueue_t *q) {
    return q->buffer_len <= 0;
}

static bool is_full(pqueue_t *q) {
    return q->buffer_len >= q->count;
}

static void add_msg(pqueue_t *q, message_t msg) {
    q->buffer[q->cursor_write] = msg;
    q->cursor_write = (q->cursor_write + 1) % q->count;
    q->buffer_len++;
}

static message_t pop_msg(pqueue_t *q) {
    message_t msg = q->buffer[q->cursor_read];
    q->cursor_read = (q->cursor_read + 1) % q->count;
    q->buffer_len--;
    return msg;
}

static void _reset(pqueue_t *q) {
    q->reset = true;

    // unblock all process
    while (! queue_empty(&q->procs)) {
        io_release(&q->procs);
    }
}

static fid_t get_free_fid() {
	fid_t fid = 0;
	while (pqueue_table[fid] != NULL && fid < NBQUEUE) {
		fid += 1;
	}
	return fid >= NBQUEUE ? -1 : fid;
}
