#ifndef MESSAGES_H
#define MESSAGES_H

#define NBQUEUE 128

/** Message queue identifier */
typedef int fid_t;

/** Here message are integer */
typedef int message_t;

/**
 * @brief Create a message queue
 *
 * @param count capacity of the queue
 *
 * @return id of the created queue
 * @return -1 on error (no more slots for queue or count <= 0)
 */
extern fid_t pcreate(int count);

/**
 * @brief Delete a message queue
 *
 * Delete the queue and set all waiting process activable.
 * All messages still in the queue are deleted.
 *
 * @param fid   id of the queue to delete
 *
 * @return 0 on success
 * @return -1 on error (invalid fid)
 */
extern int pdelete(fid_t fid);

/**
 * @brief Send a message to a queue
 *
 * @param fid       id of the queue
 * @param message   message to send
 *
 * @return 0 on success
 * @return -1 on error (invalid fid)
 */
extern int psend(fid_t fid, message_t message);

/**
 * @brief Receive a message from a queue
 *
 * @param fid       id of the queue
 * @param message   pointer to receive message
 *
 * @return 0    on success
 * @return -1   on error (invalid fid)
 */
extern int preceive(fid_t fid, message_t *message);

/**
 * @brief Reset a message queue
 *
 * Empty queue and set all waiting process activable.
 *
 * @param fid       id of the queue
 *
 * @return 0    on success
 * @return -1   on error (invalid fid)
 */
extern int preset(fid_t fid);

/**
 * @brief Get information on message queue
 *
 * @param fid       id of the queue
 * @param count
 *
 * @return 0    on success
 * @return -1   on error
 */
extern int pcount(fid_t fid, int *count);

#endif
