#include "proc.h"
#include "stdio.h"
#include "string.h"
#include "assert.h"
#include "stdbool.h"
#include "mem.h"
#include "log.h"
#include "riscv.h"
#include "timer.h"

#define STACK_HEAD (STACK_SIZE - 1)

#define WAITING_ANY_CHILD (-1)
#define USED_FRAME_COUNT (64)

enum ProcState {
    STATE_RUNNING,          // Actif
    STATE_READY,            // Activable
    STATE_WAITING_CHILD,    // Bloqué en attente d'un fils
    STATE_WAITING_IO,       // Bloqué sur IO
    STATE_SLEEPING,         // Endormi
    STATE_ZOMBIE,           // Zombie
    STATE_DEAD,             // Mort (a detruire)
    NB_STATE
};

struct context {
    word_t ra;
    word_t sp;
    word_t s0;
    word_t s1;
    word_t s2;
    word_t s3;
    word_t s4;
    word_t s5;
    word_t s6;
    word_t s7;
    word_t s8;
    word_t s9;
    word_t s10;
    word_t s11;
    word_t sstatus;
    word_t sepc;
    word_t satp;
    word_t sscratch;
};

struct proc {
    pid_t pid;              // pid from 0 to NBPROC
	char name[NAME_SIZE];   // name (debug)
    enum ProcState state;   // state of the process
    struct context context; // context saved by scheduler

    size_t ssize;           // stack size in bytes
	uint64_t * directory;	// directory to be used with this proc
	word_t **used_frames;	// frames that compose the stack

    int priority;           // priority : 0 (low) to MAXPRIO (high)

    struct proc *parent;    // parent process
    link childs;            // head of childs list

    int retval;             // return value
    bool killed;            // set to true if process was killed
    pid_t waiting;          // process waiting for

	unsigned long waiting_clock;

    /* linked lists object */
    link link_proc;
    link link_child;
    link link_waiting;
    link link_dead;
    link link_io;
	link link_clock;

    int constprio;          // constant priority to use queue as FIFO

};

/* Priority queue for ready process */
static LIST_HEAD(list_ready);

/* Queue for dead process */
static LIST_HEAD(list_dead);

/* Queue for process waiting for a child */
static LIST_HEAD(list_waiting);

/* Queue for process waiting a given clock tick */
static LIST_HEAD(list_clock);

/* Table of all process */
static proc_t *proc_table[NBPROC] = { NULL };

/* Current (= running) process */
static proc_t *current_proc = NULL;

/* dummy context for first call to process */
struct context dummy_context = {};

/**
 * @brief Do a context swap between two process.
 *
 * @param old_proc  old process
 * @param new_proc  new_process
 */
extern void ctx_sw(struct context *old_proc, struct context *new_proc);

/**
 * Wrapper for a process:
 * Call it with argument and call exit code at end.
 */
extern void wrapper_proc(void);

/* Check for valid pid and return -1 if error */
#define CHECK_PID(pid) { \
    if (!((pid) >= 0 && (pid) < NBPROC && proc_table[(pid)] != NULL)) { \
        return -1; \
    }}

/* utilities */
static void update_current_proc();
static void destroy_proc(proc_t *);
static void destroy_dead_procs();
static bool is_child(pid_t);
static void terminate_proc(proc_t *);
static void set_dead(proc_t *);
static __inline__ void activate(proc_t *);
static pid_t get_free_pid();
static void wake_on_clock();

/* Put a process to sleep in the given list */
#define SLEEP(proc, head, listfield) { \
    queue_add(proc, head, proc_t, listfield, constprio); \
    update_current_proc(); \
}

/* Wake a process from a given list and activate it */
#define WAKE(proc, listfield) { \
    queue_del(proc, listfield); \
    activate(proc); \
}

void schedule() {
    destroy_dead_procs();
	wake_on_clock();

    if (current_proc != NULL) {
        current_proc->state = STATE_READY;
        queue_add(current_proc, &list_ready, proc_t, link_proc, priority);
    }

    update_current_proc();
}

int idle(void *arg) {
    for (;;) {
		wfi();
    }

    return 0;
}

int kmmap(pid_t pid, uint64_t virt_addr, void * phys_addr, uint64_t flags, bool link) {
	CHECK_PID(pid);

	proc_t * p = proc_table[pid];

	if (link) {
		// Find the first place where this page will be stored
		int index = 0;
		while ( index < USED_FRAME_COUNT && p->used_frames[index] != 0 )
			index++;

		assert(index < USED_FRAME_COUNT);
		p->used_frames[index] = (word_t *)phys_addr;
	}

	vmem_put_pte_virtaddr(
			p->directory,
			virt_addr,
			vmem_create_pte(phys_addr, PTE_V | PTE_R | PTE_W | PTE_X | PTE_G | flags),
			KILOPAGE);
	return 0;
}

pid_t kstart(int (*pt_func)(void *), size_t ssize, int prio, const char *name, void *arg) {
	pid_t pid = get_free_pid();
	if (pid == -1) {
		return -1;
	}

    // check valid priority level
    if (prio < 1 || prio >= MAXPRIO) {
        return -1;
    }

    // Allocate memory for the data structure
    proc_t *p = kmalloc(sizeof(struct proc));
    if (p == NULL) {
        return -1;
    }

    // fill the struct with informations on process
    p->pid = pid;
    strncpy(p->name, name, NAME_SIZE);
    p->state = STATE_READY;
    p->priority = prio;
    p->killed = false;
    p->constprio = 1;
	p->directory = vmem_create_directory();

    // entry point (wrapper calling func)
	p->context.sstatus = csr_read(sstatus);
	p->context.satp = VMEM_SATP(p->pid, p->directory);
    p->context.s0 = (word_t) kexit;
    p->context.s1 = (word_t) pt_func;
    p->context.s2 = (word_t) arg;
    p->context.ra = (word_t) wrapper_proc;

    // parent process is running (current) process
    if (current_proc != NULL) {
        // set parent and add all childs
        p->parent = current_proc;
        queue_add(p, &p->parent->childs, proc_t, link_child, priority);
    }
    else {
        p->parent = NULL;
    }

    // init childs list
    INIT_LIST_HEAD(&p->childs);

    // add to process table
    queue_add(p, &list_ready, proc_t, link_proc, priority);
    proc_table[p->pid] = p;

    // Initilize virtual memory
    p->ssize = ssize;

	unsigned int nr_stack_frames = ssize / STACK_SIZE + ssize % STACK_SIZE == 0 ? 0 : 1;
	p->used_frames = kmalloc(USED_FRAME_COUNT * sizeof(word_t *));
	
	for (unsigned int index = 0; index < nr_stack_frames; index++) {
		void * frame = alloc_frame();
		kmmap(
				p->pid,
				(uint64_t)( VMEM_STACK_ADDR - KILOPAGE * (index + 1) ),
				(void *)frame,
				PTE_U,
				true);
	}

    p->used_frames[0][STACK_HEAD] = (word_t) pt_func;
    p->context.sp = (word_t)( VMEM_STACK_ADDR );

	if (current_proc == NULL || p->priority > current_proc->priority) schedule();

    return p->pid;
}

void kexit(int retval) {
    pid_t pid = kgetpid();

    // set return value
    current_proc->retval = retval;

    // exit if pid 0
    if (pid == 0) {
        // clean memory before exit
        destroy_dead_procs();
        exit(retval);
    }
    terminate_proc(current_proc);

    update_current_proc();

    /* no return */
	__builtin_unreachable();
}

int kkill(pid_t pid) {
    CHECK_PID(pid);

    // process to kill
    proc_t *p = proc_table[pid];

    // dont kill dead process
    if (p->state == STATE_ZOMBIE) {
        return -1;
    }

	
    // delete process
    p->killed = true;
    terminate_proc(p);

    // if suicide
    if (kgetpid() == pid) {
        update_current_proc();
    } else {
	    // Remove from lists
	    queue_del(p, link_proc);
    }

    return 0;
}

int kwaitpid(pid_t pid, int *retvalp) {
    if ((pid < 0 && queue_empty(&current_proc->childs)) || ((pid > 0) && !is_child(pid))){
        return -1;
    }

	// Can't wait the process currently running
	if (pid == current_proc->pid) {
		return -1;
	}

    // find zombie child
    proc_t *child = NULL;
    queue_for_each(child, &current_proc->childs, proc_t, link_child) {
        if (child->state == STATE_ZOMBIE && (pid == WAITING_ANY_CHILD || pid == child->pid)) {
            // remove child from list
            queue_del(child, link_child);

            // set correct return value
            if (child->killed) {
                retvalp = NULL;
            } else if (retvalp != NULL) {
                *retvalp = child->retval;
            }

            pid = child->pid;

            // terminate child
            set_dead(child);

            return pid;
        }
    }

    // put process in waiting list
    current_proc->state = STATE_WAITING_CHILD;
    current_proc->waiting = pid < 0 ? WAITING_ANY_CHILD : pid;
    SLEEP(current_proc, &list_waiting, link_waiting);

	// call again after being waked up to try again to get dead child
	return kwaitpid(pid, retvalp);
}

int kgetprio(pid_t pid) {
    CHECK_PID(pid);
    return proc_table[pid]->priority;
}

int kchprio(pid_t pid, int newprio) {
    CHECK_PID(pid);

    // change priority of p
    proc_t *p = proc_table[pid];
    int oldprio = p->priority;
    p->priority = newprio;

	if (pid != current_proc->pid) {
		// reinsert in process list with new priority
		queue_del(p, link_proc);
		queue_add(p, &list_ready, proc_t, link_proc, priority);
	} else {
		schedule();
	}

    return oldprio;
}

pid_t kgetpid(void) {
    if (current_proc != NULL) {
        return current_proc->pid;
    }
    else {
        return -1;
    }
}

void io_block(link *fifo) {
    current_proc->state = STATE_WAITING_IO;
    SLEEP(current_proc, fifo, link_io);
}

void io_release(link *fifo) {
    proc_t *p = queue_out(fifo, proc_t, link_io);
    activate(p);
}

void wait_clock(unsigned long clock)
{
	if (clock > current_clock()) {
		current_proc->waiting_clock = clock;
		SLEEP(current_proc, &list_clock, link_clock);
	}
}

/**
 * Wake all process that have reached the correct clock tick.
 */
void static wake_on_clock()
{
	unsigned long clock = current_clock();
	proc_t *p = NULL;
	queue_for_each(p, &list_clock, proc_t, link_clock) {
		if (p->waiting_clock <= clock) {
			WAKE(p, link_clock);
		}
	}
}

/**
 * Activate a process
 *
 * Set state to READY and reschedule if it has the highest priority.
 */
static __inline__ void activate(proc_t *proc) {
    // insert proc in scheduling queue
    proc->state = STATE_READY;
    queue_add(proc, &list_ready, proc_t, link_proc, priority);

    if (current_proc->priority < proc->priority) {
        // if higher priority activate immediately
        if (current_proc->state != STATE_ZOMBIE) {
            schedule();
        }
        else {
            // we dont want to put the zombie process back
            // in the list of runnable process
            update_current_proc();
        }
    }
    // else only insert
}

/**
 * Set a dead process to zombie state.
 *
 * Change its state, add to list of dead procs & wakeup parent
 * if necessary.
 */
static void zombify(proc_t *proc) {
    proc->state = STATE_ZOMBIE;

    // if parent is waiting for this process: wake up
    proc_t *parent = proc->parent;
    if (parent->waiting == WAITING_ANY_CHILD || parent->waiting == proc->pid) {
        if (parent->state == STATE_WAITING_CHILD) {
            WAKE(parent, link_waiting);
        }
    }
}

/**
 * Make a process die.
 *
 * It will be destroyed on next scheduler call.
 */
static void set_dead(proc_t *proc) {
    proc->state = STATE_DEAD;
    queue_add(proc, &list_dead, proc_t, link_dead, priority);
}

static void terminate_proc(proc_t *p) {
    // if parent is alive go ZOMBIE, else destroyed
    if (p->parent != NULL) {
        zombify(p);
    }
    else {
        set_dead(p);
    }

    // Update each child
    proc_t *child = NULL;
    queue_for_each(child, &p->childs, proc_t, link_child) {
        // remove parent in child
        child->parent = NULL;
        // destroy zombie child
        if (child->state == STATE_ZOMBIE) {
            destroy_proc(child);
        }
    }
}


/**
 * Switch between current and next process
 */
static void update_current_proc() {
    proc_t *old_proc = current_proc;
    current_proc = queue_out(&list_ready, proc_t, link_proc);
    assert(current_proc != NULL);

    if (old_proc == current_proc) {
        return;
    }

    current_proc->state = STATE_RUNNING;
    if (old_proc == NULL) {
        ctx_sw(&dummy_context, &current_proc->context);
    }
    else {
        ctx_sw(&old_proc->context, &current_proc->context);
    }
}

/**
 * Free memory used for a process
 */
static void destroy_proc(proc_t *p) {
    proc_table[p->pid] = NULL;

	for (unsigned int index = 0; p->used_frames[index] != NULL; index++) {
		free_frame(p->used_frames[index]);
	}

	kfree(p->used_frames, USED_FRAME_COUNT * sizeof(word_t *));
	vmem_destroy_pt(p->directory);
    /* free_frames(p->stack, p->ssize); */
    kfree(p, sizeof(proc_t));
}

/**
 * Free memory used by all dead process
 */
static void destroy_dead_procs() {
    while (! queue_empty(&list_dead)) {
        proc_t *p = queue_out(&list_dead, proc_t, link_dead);
        destroy_proc(p);
    }
}

static bool is_child(pid_t pid) {
    proc_t *child = NULL;
    queue_for_each(child, &current_proc->childs, proc_t, link_child) {
        if (child->pid == pid) {
            return true;
        }
    }
    return false;
}

static void context_dump(struct context *c) {
    const char *prefix = "    ";
    printf("%sra: %ld\n", prefix, c->ra);
    printf("%ssp: %ld\n", prefix, c->sp);
    printf("%ss0: %ld\n", prefix, c->s0);
    printf("%ss1: %ld\n", prefix, c->s1);
    printf("%ss2: %ld\n", prefix, c->s2);
    printf("%ss3: %ld\n", prefix, c->s3);
    printf("%ss4: %ld\n", prefix, c->s4);
    printf("%ss5: %ld\n", prefix, c->s5);
    printf("%ss6: %ld\n", prefix, c->s6);
    printf("%ss7: %ld\n", prefix, c->s7);
    printf("%ss8: %ld\n", prefix, c->s8);
    printf("%ss9: %ld\n", prefix, c->s9);
    printf("%ss10: %ld\n", prefix, c->s10);
    printf("%ss11: %ld\n", prefix, c->s11);
    printf("%ssstatus: %ld\n", prefix, c->sstatus);
    printf("%ssepc: %ld\n", prefix, c->sepc);
    printf("%ssatp: %ld\n", prefix, c->satp);
    printf("%ssscratch: %ld\n", prefix, c->sscratch);
}

void proc_dump(pid_t pid) {
    proc_t *p = proc_table[pid];
    printf("Process \"%s\"\n", p->name);
    printf("- pid: %d\n", p->pid);
    printf("- state: %d\n", p->state);
    printf("- ssize: %ld\n", p->ssize);
    printf("- priority: %d\n", p->priority);
    printf("- context:\n");
    context_dump(&p->context);
    printf("- parent : %s (pid %d)\n", p->parent->name, p->parent->pid);
    printf("\n");
}

static pid_t get_free_pid() {
	pid_t pid = 0;
	while (proc_table[pid] != NULL && pid < NBPROC) {
		pid += 1;
	}
	return pid >= NBPROC ? -1 : pid;
}
