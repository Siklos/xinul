/**
 * @file
 */

#ifndef PROC_H
#define PROC_H

/*
  Process
  https://fr.wikipedia.org/wiki/Processus_(informatique)
*/

#include "stdint.h"
#include "queue.h"
#include "encoding.h"
#include "stdbool.h"
#include "vmem.h"

#define NAME_SIZE 32

// Number of word_t in FRAME_SIZE, which is in bytes
#define STACK_SIZE (RISCV_PGSIZE / sizeof(word_t))

/* Maximum number of process */
#define NBPROC 30

/* Priority levels */
#define MAXPRIO         256
#define PRIORITY_IDLE   1
#define PRIORITY_LOW    32
#define PRIORITY_NORMAL 128
#define PRIORITY_HIGH   192

/* PID : Process IDentifier */
typedef int16_t pid_t;

/* RICV 64bits */
typedef uint64_t word_t;

/* Process */
typedef struct proc proc_t;

/**
 * @brief Schedule between process, by priority.
 */
extern void schedule(void);

/**
 * @brief Main (idle) process
 */
int idle(void *arg);

/**
 * @brief Create a new process.
 *
 * @return pid of the created process on success, -1 on error.
 *
 * @param pt_func       pointer to adress of program/function to start
 * @param ssize         size of program stack (in bytes)
 * @param prio          priority (1 to MAXPRIO)
 * @param name          name associated with the process
 * @param arg           argument passed to pt_func
 */
extern pid_t kstart(int (*pt_func)(void *), size_t ssize, int prio, const char *name, void *arg);

/**
 * @brief Exit the process normally and return retval to parent.
 *
 * @param retval        return value
 */
extern void kexit(int retval) __attribute__ ((noreturn));

/**
 * @brief Forcefully terminate a process.
 * If the process was waiting, remove it from queue.
 *
 * @return 0 on success, -1 on error (invalid pid).
 *
 * @param pid           pid of the process to terminate.
 */
extern int kkill(pid_t pid);

/**
 * @brief Wait and get return value from a child process.
 *
 * @return pid of the ended process on success, -1 on error or no child.
 *
 * @param pid       negative to wait for any child, positive for a given process
 * @param retvalp   pointer to returned value
 */
extern int kwaitpid(pid_t pid, int *retvalp);

/**
 * @brief Get the priority of a process.
 *
 * @return Priority of the process or -1 on error (invalid pid).
 *
 * @param pid   pid of the process
 */
extern int kgetprio(pid_t pid);

/**
 * @brief Change the priority of a process.
 *
 * If the process was waiting, replace him in the queue with new priority.
 *
 * @return old priority or -1 on error.
 *
 * @param pid       pid of the process
 * @param newprio   new priority of the process
 */
extern int kchprio(pid_t pid, int newprio);

/**
 * @brief Get priority of the calling process.
 *
 * @return pid of the current (running) process.
 */
extern pid_t kgetpid(void);

/**
 * Dump the content of a process data stucture.
 * (debug)
 */
extern void proc_dump(pid_t pid);

/**
 * Put the process to sleep until the given
 * number of clock interruptions is reached.
 */
extern void wait_clock(unsigned long clock);

extern void io_block(link *fifo);
extern void io_release(link *fifo);


int kmmap(pid_t pid, uint64_t virt_addr, void * phys_addr, uint64_t flags, bool link);

#endif
