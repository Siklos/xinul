/*
 * Projet PCSEA RISC-V
 *
 * Benoît Wallon <benoit.wallon@grenoble-inp.org> - 2019
 * Mathieu Barbe <mathieu@kolabnow.com> - 2019
 *
 * See license for license details.
 */

#include "sbi.h"

void sbi_call_set_timer(uint64_t delta)
{
	SBI_CALL_1(SBI_SET_TIMER, delta);
}


void sbi_call_ack_timer_interrupt()
{
	SBI_CALL_0(SBI_ACK_TIMER_INTERRUPT);
}

