/*
 * Projet PCSEA RISC-V
 *
 * Benoît Wallon <benoit.wallon@grenoble-inp.org> - 2019
 * Mathieu Barbe <mathieu@kolabnow.com> - 2019
 *
 * See license for license details.
 */

#include "assert.h"

#include "encoding.h"
#include "riscv.h"
#include "sbi.h"
#include "device.h"


/*
 * sbi_call_set_timer
 *
 * Cette fonction configure la prochaine interruption timer supervisor delta ms dans le future.
 *
 * Cette appel sbi va :
 * - activer les interruption timer machine;
 * - aquitter l'interruption timer supervisor;
 * - configurer le registre timecmp du clint.
 *
 *@param delta : réglage de la prochaine interruption à cur + delta ms.
 */
void handle_sbi_set_timer(uint64_t delta)
{
	/**
	 * Il faut acquiter l'interruption timer supervisor ici car le bit SIP_STIP est read-only en mode
	 * supervisor. Grâce au SBI call, nous somme ici en mode machine.
	 */
	clint_dev->clk_freq = 10000 * delta;
}

void handle_sbi_ack_timer_interrupt()
{
	csr_clear(mip, MIP_STIP);
}


uint64_t handle_sbi_call(
		uint64_t call_no, uintptr_t arg0, uintptr_t arg1, uintptr_t arg2)
{
	csr_write(mepc, csr_read(mepc) + 4);
	switch (call_no) {
		case SBI_SET_TIMER:
			handle_sbi_set_timer(arg0);
			break;
		case SBI_ACK_TIMER_INTERRUPT:
			handle_sbi_ack_timer_interrupt();
			break;
		default:
			die("machine mode: sbi call %ld\n", call_no);
			break;
	}

	return 0;
}
