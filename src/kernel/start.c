/*
 * Projet PCSEA RISC-V
 *
 * Benoît Wallon <benoit.wallon@grenoble-inp.org> - 2019
 * Mathieu Barbe <mathieu@kolabnow.com> - 2019
 *
 * See license for license details.
 */

#include "stdio.h"
#include "assert.h"
#include "stddef.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"

#include "sbi.h"
#include "vga.h"
#include "splash.h"
#include "riscv.h"

#include "vmem.h"
#include "mem.h"
#include "riscv.h"

#include "proc.h"

#include "tests/tests.h"

int kernel_start()
{
	splash_screen();
	splash_vga_screen();

	// Initialize memory allocator
	init_allocator();

	// Setup kernel directory
	// This is a 1:1 translation, really important so that memory management works properly.
	init_vmem_kernel();

	sbi_call_set_timer(5);

#ifdef HAS_TESTS
    kstart(run_tests, 128, 128, "tests", NULL);
#else
    kstart(idle, 128, PRIORITY_IDLE, "idle", NULL);
#endif

	return 0;
}
