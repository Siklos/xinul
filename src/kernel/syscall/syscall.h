#ifndef __SYSCALL_H__
#define __SYSCALL_H__


uint64_t handle_syscall(uint64_t call_no, uintptr_t arg0, uintptr_t arg1, uintptr_t arg2);

#endif /* __SYSCALL_H__ */
