#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include "syscall_num.h"
#include "encoding.h"
#include "riscv.h"
#include "proc.h"
#include "cons_write.h"

uint64_t handle_syscall(
		uint64_t call_no, uintptr_t arg0, uintptr_t arg1, uintptr_t arg2) {
	// ecall dont move 
	csr_write(sepc, csr_read(sepc) + 4);
	switch (call_no) {
		case SYSC_exit:
			kexit(arg0);
			break;
		case SYSC_cons_write:
			cons_write((char *)arg0, arg1);
			break;
		default:
			printf("Unsupported syscall : %lu\n", call_no);
			assert(0);
	}

	csr_clear(sip, SIP_STIP);
	return 0;
}
