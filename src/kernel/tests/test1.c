/*******************************************************************************
 * Test 1
 *
 * Demarrage de processus avec passage de parametre
 * Terminaison normale avec valeur de retour
 * Attente de terminaison (cas fils avant pere et cas pere avant fils)
 ******************************************************************************/
#include "stdio.h"
#include "assert.h"
#include "proc.h"

#define DUMMY_VAL 78

int dummy2(void *arg)
{
        printf(" 5");
		long int arg_val = (long int)arg;
        assert(arg_val == DUMMY_VAL + 1);
        return 4;
}

int dummy1(void *arg) {
        printf("1");
		long int arg_val = (long int)arg;
        assert(arg_val == DUMMY_VAL);
        return 3;
}

int run_test1(void)
{
        int pid1;
        int r;
        int rval;

        pid1 = kstart(dummy1, 4000, 192, "dummy1",(void *) DUMMY_VAL);
        assert(pid1 >= 0);
        printf(" 2");
        r = kwaitpid(pid1, &rval);
        assert(r == pid1);
        assert(rval == 3);
        printf(" 3");
        pid1 = kstart(dummy2, 4000, 100, "dummy2", (void *) (DUMMY_VAL + 1));
        assert(pid1 >= 0);
        printf(" 4");
        r = kwaitpid(pid1, &rval);
        assert(r == pid1);
        assert(rval == 4);
        printf(" 6.\n");
        return 0;
}

