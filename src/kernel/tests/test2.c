#include "stdio.h"
#include "assert.h"
#include "proc.h"

int procExit(void *args)
{
        printf(" 5");
        kexit((long int) args);
        assert(0);
        return 0;
}

int procKill(void *args)
{
        printf(" X");
        return (long int)args;
}

int run_test2()
{
        int rval;
        int r;
        int pid1;
        long int val = 45;

        printf("1");
        pid1 = kstart(procKill, 4000, 100, "procKill", (void *) val);
        assert(pid1 > 0);
        printf(" 2");
        r = kkill(pid1);
        assert(r == 0);
        printf(" 3");
        r = kwaitpid(pid1, &rval);
        assert(rval == 0);
        assert(r == pid1);
        printf(" 4");
        pid1 = kstart(procExit, 4000, 192, "procExit", (void *) val);
        assert(pid1 > 0);
        printf(" 6");
        r = kwaitpid(pid1, &rval);
        assert(rval == val);
        assert(r == pid1);
        assert(kwaitpid(kgetpid(), &rval) < 0);
        printf(" 7.\n");

		return 0;
}

