#include "stdio.h"
#include "assert.h"
#include "proc.h"

int prio5(void *arg)
{
        /* Arg = priority of this proc. */
        long int r;

        assert(kgetprio(kgetpid()) == (long int) arg);
        printf(" 7");
        r = kchprio(kgetpid(), 64);
        assert(r == (long int)arg);
        printf("error: I should have been killed\n");
        assert(0);
        return 0;
}

int prio4(void *arg)
{
        /* arg = priority of this proc. */
        long int r;

        assert(kgetprio(kgetpid()) == (long int) arg);
        printf("1");
        r = kchprio(kgetpid(), 64);
        assert(r == (long int) arg);
        printf(" 3");
        return 0;
}


int run_test3(void)
{
        int pid1;
        long int p = 192;
        int r;

        assert(kgetprio(kgetpid()) == 128);
        pid1 = kstart(prio4, 4000, p, "prio4", (void *) p);
        assert(pid1 > 0);
        printf(" 2");
        r = kchprio(kgetpid(), 32);
        assert(r == 128);
        printf(" 4");
        r = kchprio(kgetpid(), 128);
        assert(r == 32);
        printf(" 5");
        assert(kwaitpid(pid1, 0) == pid1);
        printf(" 6");

        assert(kgetprio(kgetpid()) == 128);
        pid1 = kstart(prio5, 4000, p, "prio5", (void *) p);
        assert(pid1 > 0);
        printf(" 8");
        r = kkill(pid1);
        assert(r == 0);
        assert(kwaitpid(pid1, 0) == pid1);
        printf(" 9");
        r = kchprio(kgetpid(), 32);
        assert(r == 128);
        printf(" 10");
        r = kchprio(kgetpid(), 128);
        assert(r == 32);
        printf(" 11.\n");

		return 0;
}


