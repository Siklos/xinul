#include "stdio.h"
#include "assert.h"
#include "proc.h"
#include "riscv.h"

static const int loop_count0 = 5;
static const int loop_count1 = 20;

int busy1(void *arg)
{
	(void)arg;
	while (1) {
		int i, j;

		printf(" A");
		for (i=0; i<loop_count1; i++) {
			wfi();
			for (j=0; j<loop_count0; j++);
		}
	}
	return 0;
}

/* assume the process to suspend has a priority == 64 */
int busy2(void *arg)
{
	int i;

	for (i = 0; i < 3; i++) {
		int k, j;

		printf(" B");
		for (k=0; k<loop_count1; k++) {
			wfi();
			for (j=0; j<loop_count0; j++);
		}
	}
	i = kchprio((long int) arg, 16);
	assert(i == 64);
	return 0;
}

int run_test4(void)
{
	long int pid1, pid2;
	int r;
	long int arg = 0;

	assert(kgetprio(kgetpid()) == 128);
	pid1 = kstart(busy1, 4000, 64, "busy1",(void *) arg);
	assert(pid1 > 0);
	pid2 = kstart(busy2, 4000, 64, "busy2", (void *) pid1);
	assert(pid2 > 0);
	printf("1 -");
	r = kchprio(kgetpid(), 32);
	assert(r == 128);
	printf(" - 2");
	r = kkill(pid1);
	assert(r == 0);
	assert(kwaitpid(pid1, 0) == pid1);
	r = kkill(pid2);
	assert(r < 0); /* kill d'un processus zombie */
	assert(kwaitpid(pid2, 0) == pid2);
	printf(" 3");
	r = kchprio(kgetpid(), 128);
	assert(r == 32);
	printf(" 4.\n");

	return 0;
}

