/*******************************************************************************
 * Test 8
 *
 * Creation de processus se suicidant en boucle. Test de la vitesse de creation
 * de processus.
 ******************************************************************************/
#include "stdio.h"
#include "assert.h"
#include "proc.h"
#include "riscv.h"

int psuicide()
{
	kkill(kgetpid());
	assert(0);
	return 0;
}

int suicide_launcher()
{
	int pid1;
	pid1 = kstart(psuicide, 4000, 192, "psuicide", 0);
	assert(pid1 > 0);
	return pid1;
}

int run_test8(void)
{
        unsigned long long tsc1;
        unsigned long long tsc2;
        int i, pid, count;
        int r;

        assert(kgetprio(kgetpid()) == 128);

        /* Le petit-fils va passer zombie avant le fils mais ne pas
           etre attendu par waitpid. Un nettoyage automatique doit etre
           fait. */
	pid = kstart(suicide_launcher, 4000, 129, "suicide_launcher", 0);
	assert(pid > 0);
        assert(kwaitpid(pid, &r) == pid);
        assert(kchprio((int)r, 192) < 0);

        count = 0;
        __asm__ __volatile__("rdcycle %0":"=r"(tsc1));
        do {
                for (i=0; i<10; i++) {
			pid = kstart(suicide_launcher, 4000, 200,
				     "suicide_launcher", 0);
			assert(pid > 0);
                        assert(kwaitpid(pid, 0) == pid);
                }
                wfi();
                count += i;
                __asm__ __volatile__("rdcycle %0":"=r"(tsc2));
        } while ((tsc2 - tsc1) < 1000000000);
        printf("%lld cycles/process.\n", (tsc2 - tsc1) /  (2 * (unsigned)count));
        return 0;
}

