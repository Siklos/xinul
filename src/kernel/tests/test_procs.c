/**
 * Basic tests on process.
 */

#include "proc.h"
#include "stdio.h"
#include "assert.h"

int proc(void *arg) {
	assert(arg != NULL);
	int *argint = (int *) arg;

	for (int i = 0; i < 10; i++) {
		printf("pid %d, arg: %d (loop %d)\n", kgetpid(), *argint, i);
		schedule();
	}

	return 0;
}

int proc1(void *arg) {
	return proc(arg);
}

int proc2(void *arg) {
    return proc(arg);
}

int hello(void *arg) {
	char *str = (char *) arg;
	printf("hello %s\n", str);
	/* proc_dump(kgetpid()); */
	return 0;
}

int procgen(void *arg) {
	int *n = (int *) arg;
	printf("proc gen n=%d\n", *n);
	for (int i = 0; i < *n; i++) {
		kstart(hello, 128, PRIORITY_NORMAL, "hello", "world");
		/* schedule(); */
	}

	return 0;
}


int return_n(void *arg) {
	int *n = (int *) arg;
	return *n;
}

int return42(void *arg) {
	return 42;
}

int suicide(void *arg) {
	kkill(kgetpid());

	// never happen
	return 1;
}

int proc_waitpid(void *arg) {
	int retval = -6;
	printf("retval before: %d\n", retval);

	int n = 42;
	pid_t pid = kstart(return_n, 128, PRIORITY_NORMAL, "return 42", &n);
	kwaitpid(pid, &retval);
	printf("retval after return: %d\n", retval);

	int n2 = -777;
	pid_t pid2 = kstart(return_n, 128, PRIORITY_NORMAL, "return -777", &n2);
	kwaitpid(pid2, &retval);
	printf("retval after return 2: %d\n", retval);

	pid_t pid3 = kstart(suicide, 128, PRIORITY_NORMAL, "suicide", NULL);
	int *ptr = NULL;
	kwaitpid(pid3, ptr);
	printf("retval after kill: %p\n", ptr);


	return 0;
}

void run_test_procs(void)
{
	int arg1 = 42;
	int arg2 = 33;
	kstart(proc1, 128, PRIORITY_HIGH, "proc 1", &arg1);
	kstart(proc2, 128, PRIORITY_HIGH, "proc 2", &arg2);
	
	int n = 4;
	kstart(procgen, 128, PRIORITY_HIGH, "procgen", &n);

	kstart(proc_waitpid, 128, PRIORITY_HIGH, "proc_waitpid", NULL);
}
