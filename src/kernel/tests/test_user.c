#include "stdio.h"
#include "assert.h"
#include "proc.h"
#include "mem.h"
#include "userspace_apps.h"

int run_test_user(void)
{
	pid_t pid1;
	struct user_start_arg * args = (struct user_start_arg *)kmalloc(sizeof(struct user_start_arg));
	*args = (struct user_start_arg){ .name="test0", .args=0 };
	pid1 = kstart(user_start, 4000, PRIORITY_HIGH, "user_start_autotest",
			(void *)(args));

	int ret = 0;
	kwaitpid(pid1, &ret);
	assert(ret == 0);
	return 0;
}


