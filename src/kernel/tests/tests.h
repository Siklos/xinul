#ifndef __TESTS_H__
#define __TESTS_H__

extern void run_test0(void);
extern void run_test1(void);
extern void run_test2(void);
extern void run_test3(void);
extern void run_test4(void);
extern void run_test8(void);
extern void run_test10(void);
extern void run_test12(void);
extern void run_test_procs(void);
extern void run_test_user(void);

int run_tests(void * arg) {
	(void)arg;

#ifdef TEST_0
    printf("test0 begin:\n");
    run_test0();
    printf("test0 end.\n");
#endif

#ifdef TEST_USER
    printf("test_user begin:\n");
	run_test_user();
    printf("test_user end.\n");
#endif

#ifdef TEST_1
    printf("test1 begin:\n");
    run_test1();
    printf("test1 end.\n");
#endif

#ifdef TEST_2
    printf("test2 begin:\n");
    run_test2();
    printf("test2 end.\n");
#endif

#ifdef TEST_3
    printf("test3 begin:\n");
    run_test3();
    printf("test3 end.\n");
#endif

#ifdef TEST_4
    printf("test4 begin:\n");
    run_test4();
    printf("test5 end.\n");
#endif

#ifdef TEST_8
    printf("test8 begin:\n");
    run_test8();
    printf("test8 end.\n");
#endif

#ifdef TEST_10
    printf("test10 begin:\n");
    run_test10();
    printf("test10 end.\n");
#endif

#ifdef TEST_12
    printf("test12 begin:\n");
    run_test12();
    printf("test12 end.\n");
#endif

#ifdef TEST_PROCS
	run_test_procs();
#endif

	return 0;
}

#endif /* __TESTS_H__ */
