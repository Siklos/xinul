#include "assert.h"
#include "riscv.h"

#include "trap.h"
#include "timer.h"
#include "sbi.h"
#include "syscall.h"


/**
 * Supervisor interruption/exception calls handler
 * 
 * Cases are determined by the register scause.
 * 
 * @param scause Interruption/exception cause
 * @param sepc PC address
 * @param tf GP registers
 */
void strap_handler(uintptr_t scause, void *sepc, struct trap_frame *tf)
{
	if (scause & INTERRUPT_CAUSE_FLAG) {
		// Interruption cause
		uint8_t interrupt_number = scause & ~INTERRUPT_CAUSE_FLAG;
		switch (scause & ~INTERRUPT_CAUSE_FLAG) {
			case intr_s_timer:
				handle_stimer_interrupt();
				break;
			case intr_m_external:
				csr_clear(mip, MIP_MEIP);
				break;
			case intr_s_external:
				csr_clear(sip, MIP_SEIP);
				break;
			default:
				die(
						"supervisor mode: unhandlable interrupt trap %d : %s @ %p",
						interrupt_number, interruption_names[interrupt_number], sepc
				);
				break;
		}
	} else {
		// Exception cause
		switch (scause) {
			case CAUSE_SUPERVISOR_ECALL:
				// call the function with the saved registers a7, a0, a1 and a2
				handle_sbi_call(tf->a7, tf->a0, tf->a1, tf->a2);
				break;
			case CAUSE_USER_ECALL:
				// Call the syscall handler
				handle_syscall(tf->a7, tf->a0, tf->a1, tf->a2);
				break;
			default:
				blue_screen(tf);
				// no return
		}
	}
}
