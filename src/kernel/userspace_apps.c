#include <userspace_apps.h>
#include <stdint.h>
#include "encoding.h"
#include "mem.h"
#include "vmem.h"
#include "proc.h"
#include <string.h>

extern void userstart_wrapper(void * app_arg);

const struct uapps *find_app(const char *name) {
	for (unsigned int index = 0; symbols_table[index].name != NULL; index++) {
		if ( strcmp(symbols_table[index].name, name) == 0 ) {
			return symbols_table + index;
		}
	}

	return NULL;
}

int user_start(void * p_arg) {

	struct user_start_arg * arg = (struct user_start_arg *)p_arg;

	const struct uapps * app = find_app(arg->name);
	void ** used_frames = NULL;
	void * heap = NULL;

	if ( app == NULL ) {
		return -1;
	}

	pid_t current_pid = kgetpid();
	uint64_t app_size = (uint64_t)app->end - (uint64_t)app->start;

	uint64_t nr_pages = app_size / RISCV_PGSIZE + ( app_size % RISCV_PGSIZE == 0 ? 0 : 1 );
	used_frames = (void **)kmalloc(nr_pages * sizeof(void *));

	// Allocate pages and copy the code to the correct place
	for (uint64_t index = 0; index < nr_pages; index++) {
		used_frames[index] = alloc_frame();
		kmmap(
				current_pid,
				(uint64_t)( VMEM_USERSPACE_ADDR + RISCV_PGSIZE * index ),
				used_frames[index],
				PTE_U,
				true);

		// Copy code to memory
		for (uint64_t instr_index = 0; instr_index < RISCV_PGSIZE / sizeof(uint64_t); instr_index++) {
			((uint64_t *)used_frames[index])[instr_index] =
				((uint64_t *)( app->start + RISCV_PGSIZE * index ))[instr_index];
		}
	}


	// Allocate heap to be used by the process
	heap = alloc_frame();
	kmmap(
			current_pid,
			(uint64_t)(VMEM_USER_HEAP_ADDR),
			heap,
			PTE_U,
			true);

	// Run the app
	userstart_wrapper(arg->args);
	return 0;
}
