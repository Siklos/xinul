#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "vmem.h"
#include "mem.h"
#include "riscv.h"

void vmem_set_ppn(uint64_t * page_table, uint16_t asid)
{
	csr_write(satp, VMEM_SATP(asid, page_table));
}

uint64_t * vmem_create_pt()
{
	uint64_t * page_table = (uint64_t *)vmem_create_page();

	// Default initialization for each entry
	for ( unsigned int i = 0 ; i < NR_PTE ; i++ ) {
		page_table[i] = 0;
	}
	return page_table;
}

uint64_t * vmem_create_directory()
{
	uint64_t * directory = vmem_create_pt();

	// Set up kernel gigapage and io pages
	uint64_t pte_io_1 = vmem_create_pte(VMEM_IO_ADDR, PTE_V | PTE_R | PTE_W | PTE_G);
	uint64_t pte_io_2 = vmem_create_pte(VMEM_IO_ADDR + GIGAPAGE, PTE_V | PTE_R | PTE_W | PTE_G);

	// Set up kernel page table
	for ( uint64_t mega_index = 0; mega_index < NR_PTE; mega_index++ ) {

		if ( mega_index != 0 ) {
			uint64_t addr = VMEM_KERNEL_ADDR + (MEGAPAGE * mega_index);
			uint64_t pte = vmem_create_pte((void *)addr, PTE_V | PTE_G | PTE_X | PTE_R | PTE_W);
			vmem_put_pte_virtaddr(directory, addr, pte, MEGAPAGE);
		} else {
			vmem_put_pte_virtaddr(directory, 0, 0, KILOPAGE);

			// Set up each kilo page except for the first one
			for ( uint64_t kilo_index = 1; kilo_index < NR_PTE; kilo_index++ ) {
				uint64_t addr = VMEM_KERNEL_ADDR + (RISCV_PGSIZE * kilo_index);
				uint64_t pte = vmem_create_pte((void *)addr, PTE_V | PTE_G | PTE_X | PTE_R | PTE_W);
				vmem_put_pte_virtaddr(directory, addr, pte, KILOPAGE);
			}
		}
	}
	// USER SPACE @ directory[1]
	directory[2] = pte_io_1;
	directory[3] = pte_io_2;

	return directory;
}

void * vmem_create_page()
{
	return alloc_frame();
}

const uint64_t ALL_FLAGS = PTE_V | PTE_R | PTE_W | PTE_X | PTE_U | PTE_G | PTE_A | PTE_D | PTE_SOFT;
static uint64_t * addr_from_pte(uint64_t pte) {

	return (uint64_t *)( (pte & ~ALL_FLAGS) << 2 );
}

uint64_t vmem_create_pte(void * addr, uint64_t flags)
{
	uint64_t int_addr = (uint64_t)addr;
	assert(!((int_addr >> 2) & ALL_FLAGS));
	return (int_addr >> 2) | flags;
}

void init_vmem_kernel()
{
	uint64_t * page_table = vmem_create_directory();
	page_table[1] = vmem_create_pte(VMEM_USERSPACE_ADDR, PTE_V | PTE_R | PTE_W | PTE_X | PTE_G);
	vmem_set_ppn(page_table, 0);
}

void vmem_put_pte_virtaddr(uint64_t * page_table, uint64_t virt_addr, uint64_t pte, page_size_t size) {
	unsigned int giga_index = virt_addr / GIGAPAGE;
	unsigned int mega_index = (virt_addr % GIGAPAGE) / MEGAPAGE;
	unsigned int kilo_index = (virt_addr % MEGAPAGE) / KILOPAGE;


	// Handle gigapages right away
	if (size == GIGAPAGE) {
		if (( page_table[giga_index] & PTE_V ) == 0) {
			page_table[giga_index] = pte;
		}
		return;
	}

	// From now on, only MEGAPAGE and KILOPAGE need to be considered

	// Properly initialize gigapages if needed.
	if (( page_table[giga_index] & PTE_V ) == 0) {
		uint64_t * table = vmem_create_pt();
		page_table[giga_index] = vmem_create_pte((void *)table, PTE_G | PTE_V);
	}


	// Go one level deeper and put the value at the correct spot
	assert( PTE_TABLE(page_table[giga_index]) );

	uint64_t * mega_table = addr_from_pte(page_table[giga_index]);

	if (size == KILOPAGE) {
		// Ensure to initialize next level properly, and if needed
		if (( mega_table[mega_index] & PTE_V ) == 0) {
			uint64_t * table = vmem_create_pt();
			mega_table[mega_index] = vmem_create_pte((void *)table, PTE_G | PTE_V);
		}

		assert(PTE_TABLE(mega_table[mega_index]));
		uint64_t * kilo_table = addr_from_pte(mega_table[mega_index]);

		kilo_table[kilo_index] = pte;
	} else {
		mega_table[mega_index] = pte;
	}
}

void vmem_destroy_pt(uint64_t * pt) {
	for (unsigned int index = 0; index < NR_PTE; index++) {
		if (PTE_TABLE(pt[index])) {
			uint64_t * addr = addr_from_pte(pt[index]);
			vmem_destroy_pt(addr);
		}
	}

	free_frame((void *)pt);
}
