#ifndef __VMEM_H__
#define __VMEM_H__

#include "encoding.h"

#define NR_PTE 0x200
#define SATP_PPN_SHIFT RISCV_PGSHIFT
#define SATP_ASID_SHIFT 44
#define VMEM_KERNEL_ADDR			NULL
#define VMEM_USERSPACE_ADDR			((void *)0x40000000)
#define VMEM_USER_HEAP_ADDR			((void *)0x50000000)
#define VMEM_STACK_ADDR				((void *)0x60000000)
#define VMEM_IO_ADDR				((void *)0x80000000)
#define VMEM_SATP(asid, pt) (SATP_MODE_SV39 | ((uint64_t)(asid) << SATP_ASID_SHIFT) | ((uint64_t)(pt) >> SATP_PPN_SHIFT))

typedef enum {
	KILOPAGE = RISCV_PGSIZE,
	MEGAPAGE = RISCV_PGSIZE << 9,
	GIGAPAGE = RISCV_PGSIZE << 18
} page_size_t;

uint64_t * vmem_create_pt();
uint64_t * vmem_create_directory();

uint64_t vmem_create_pte(void * addr, uint64_t flags);
void * vmem_create_page();

void vmem_set_ppn(uint64_t * page_table, uint16_t asid);

void init_vmem_kernel();

void vmem_destroy_pt(uint64_t * pt);
void vmem_put_pte_virtaddr(uint64_t * page_table, uint64_t virt_addr, uint64_t pte, page_size_t size);
#endif // __VMEM_H__
