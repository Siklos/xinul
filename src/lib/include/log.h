#ifndef LOG_H
#define LOG_H

#ifdef DEBUG
    #define log_debug(...) printf(__VA_ARGS__)
#else
    #define log_debug(...) ;
#endif

#endif
