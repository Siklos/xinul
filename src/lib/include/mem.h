/*
 * Copyright (C) 2005 Simon Nieuviarts
 *
 * Memory allocator for kernel data.
 */
#ifndef __MEM_H__
#define __MEM_H__

#include "stddef.h"

// Kernel allocator

/**
 * Function top be called top initialize kernel memory allocator.
 * Writes many things to memory, and sets up memory spaces for future allocations.
 */
void init_allocator();

/**
 * Allocates length bytes init memory, and returns a pointer to it.
 */
void * kmalloc(unsigned long length);

/**
 * Frees a previously allocated memory.
 */
void kfree(void *zone, unsigned long length);

// Frame allocator

/**
 * Allocates a frame of RISCV_PGSIZE and returns a pointer to it.
 */
void * alloc_frame();

/**
 * Frees a previously allocated frame.
 */
void free_frame(void *zone);

#endif
