# Roadmap

- Appel `start(name, ..., args)`
- Syscall -> mode kernel
- Appel `kstart(user_start, ..., {name=name, args=args})`
- Dans `user_start`
	- Chercher `name` dans la table des apps
	- Charger son code en mémoire
		- Le trouver
		- Le déplacer
	- Mettre en place la virtualisation
		- Créer la table des pages
		- Mettre a jour le registre spac
		- Map son code à 0x40000000
		- Map son tas à 0x50000000
			- Allouer une page
			- La mappper
	- -> mode user
		- Voir `enter_supervisor_mode`
	- Appeler `main(args)`
