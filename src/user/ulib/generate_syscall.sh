#!/bin/bash

printf "// Sycalls for Xinul\n#include \"syscall_num.h\"\n\n" > syscall.S

SYSCALLS=("exit" "cons_write")

for s in ${SYSCALLS[*]}
do
	cat >> syscall.S <<- EOM
.global $s
$s:
    li a7, SYSC_$s
    ecall
    ret

EOM
done

