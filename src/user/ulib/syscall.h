/*
 * Damien Dejean - Gaëtan Morin
 * Mathieu Barbe
 * Ensimag, Projet Système 2010
 *
 * XUNIL
 * Headers de la bibliothèque d'appels systèmes.
 */
#ifndef ___SYSCALL_H___
#define ___SYSCALL_H___


extern void exit(int out);

#endif
